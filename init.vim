syntax on                      " 语法高亮
filetype on
filetype plugin on
filetype plugin indent on      " 开启自动识别文件类型,并根据文件类型加载不同的插件和缩进规则
set number                     " 显示行号
set relativenumber             " 显示相对行号
set autoindent
set smartindent
set softtabstop=4
set shiftwidth=4               " 设置位移宽度为4
set tabstop=4                  " 设置缩进宽度为4
set expandtab                  " 将缩进替换为空格
set nobackup                   " 不生成backup文件
set scrolloff=10               " 设置滚动时始终显示上下10行
set nowrap                     " 禁止折行
set incsearch                  " 增量式搜索
set ignorecase                 " 搜索时大小写不敏感
set smartcase                  " 搜索时对首字母大小写敏感
set showcmd                    " 显示键入的命令前缀
set showmode                   " 显示当前模式(插入、可视等)
set hlsearch                   " 高亮搜索结果
set history=1000               " 设置命令历史记录为1000
set wildmenu                   " 设置tab补全
set wildmode=list:longest      " 使tab补全类似于Bash
set encoding=utf-8             " 设置编码方式为UTF-8
set showmatch                  " 当光标置于成对符号(例如括号)时,高亮匹配的符号对
exec "nohlsearch"
set mouse=a                    " 支持鼠标
set foldmethod=indent          " 代码折叠
set foldcolumn=0               " 设置代码区域的宽度
setlocal foldlevel=1           " 设置折叠层数为
set foldlevelstart=99          " 打开文件是默认不折叠代码
set cursorline                 " 光标所在的当前行高亮
set cursorcolumn               " 高亮当前列



let g:mapleader = " "                   " leader 改为空格

cmap w!! w !sudo tee >/dev/null %       " w!!写入只读文件

map tq :tabclose                        " 标签页操作  关闭标签页 tabe close
map tmh :-tabmove                       " 移动标签页 tabe move h/j
map tml :+tabmove

noremap ta :tabe<CR>                    " ta 为新建标签页,th 和 tl 分别向左右跳转
noremap th :-tabnext<CR>
noremap tl :+tabnext<CR>

let &t_SI = "\]50;CursorShape=1\x7"     " 根据不同模式,改变光标样式
let &t_SR = "\]50;CursorShape=2\x7"
let &t_EI = "\]50;CursorShape=0\x7"

noremap j gj
noremap k gk
noremap gj j
noremap gk k
noremap J 5gj
noremap K 5gk

noremap <leader><CR> :nohlsearch<CR>    " 取消高亮映射 空格(<leader>)和回车(<CR>)

noremap s <nop>                         " 分屏(split)
noremap sl :set splitright<CR>:vsplit<CR>
noremap sh :set nosplitright<CR>:vsplit<CR>
noremap sj :set splitbelow<CR>:split<CR>
noremap sk :set nosplitbelow<CR>:split<CR>

noremap <leader>l <c-w>l                " 使用空格加方向键实现分屏之间的跳转
noremap <leader>h <c-w>h
noremap <leader>j <c-w>j
noremap <leader>k <c-w>k

noremap <leader>L <c-w>L                " 使用空格加大写方向键将当前分屏放置到指定方向的最边缘
noremap <leader>H <c-w>H
noremap <leader>J <c-w>J
noremap <leader>K <c-w>K


noremap <leader>o o<Esc>k               " 空格加小写字母 o:向下插入空行
noremap <leader>O O<Esc>j               " 空格加大写字母 O:向上插入空行
noremap <leader>w :w<CR>                " 空格加小写字母 w:保存文档
noremap <leader>q :q<CR>                " 空格加小写字母 q:退出 Neovim
noremap Y "+y                           " 大写字母 Y:复制到系统剪切板

noremap = nzz                           " 查找 zz 可以将当前行居中,继续查找设置为更常用(输入法常用)的 = 和 - 键:
noremap - Nzz

nnoremap R :source $MYVIMRC<CR>         " 普通模式下大写字母 R:刷新 Neovim 配置
"0 和 $ 默认情况下分别表示实际行的行首和行尾
nnoremap g0 0
nnoremap 0 g0
nnoremap $ g$
nnoremap g$ $




silent !mkdir -p ~/.config/nvim/tmp/backup   " 编辑 保留记录,退出后再次打开依然可以 undo
silent !mkdir -p ~/.config/nvim/tmp/undo
set backupdir=~/.config/nvim/tmp/backup,.
set directory=~/.config/nvim/tmp/backup,.
if has('persistent_undo')
  set undofile
  set undodir=~/.config/nvim/tmp/undo,.
endif

noremap <leader>e  :Explore<CR>              " 当前窗口下打开
noremap <leader>ve :Vexplore<CR>             " 竖直分割窗口打开
noremap <leader>se :Sexplore<CR>             " 水平分割窗口打开

au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif  " 恢复光标
